import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrl: './app.component.css'
})
export class AppComponent implements OnInit{
  title = 'datingappfe';
  users: any;

  constructor(private http : HttpClient) {}

  ngOnInit(): void {
    this.http.get('https://localhost:5000/api/user').subscribe({
      next: response => this.users = response,
      error: err => console.log(err),
      complete: () => console.log('completed')
    });
  }
}
